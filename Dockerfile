#
# Consider this a temporary starting point, until we get a better, custom-built
# base container for the JGI. As it stands, this is over 170 MB for 'hello world'!
FROM debian:jessie
#
# You can try building from an alpine:3.5 base if you want to optimize your image.
# Read https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management for details
# on how to manage packages on alpine linux
#FROM alpine:3.5

#
# Please update this metadata :-)
Maintainer Kecia Duffy, kmduffy@lbl.gov

#
# If you need to add more software, try to follow this style:
# - use --no-install-recommends to reduce bloat
# - use command-chaining to keep it all in one cache layer
# - use purge to remove stuff afterwards if you can
#
# Set the DEBIAN_FRONTEND environment variable as shown to avoid problems
# being prompted for input
#
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && \
    apt-get install -y wget ca-certificates --no-install-recommends && \
	apt-get install make && \
	wget https://github.com/hyattpd/Prodigal/releases/download/v2.6.3/prodigal.linux && \
    chmod 755 prodigal.linux && \
    mv prodigal.linux /usr/bin/prodigal && \
    apt-get purge -y --auto-remove




